// ==UserScript==
// @name        PTE Decorator
// @namespace   http://portilho.com/
// @version     1.1-SNAPSHOT
// @description PTE Decorator
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js
// @require     http://cdnjs.cloudflare.com/ajax/libs/lodash.js/2.4.1/lodash.js
// @require     https://jsturbo.googlecode.com/hg/dist/jsturbo-0.0.6.js
// @match       https://*.petrobras.com.br/PTPE/*
// ==/UserScript==
function RegrasStiff() {}


RegrasStiff.prototype.agrupaHorasPorData = function(arrayDataHorasObj) {
    var groupByData = _.groupBy(arrayDataHorasObj, function(it) { return it.data; });
    var horasPorDataObj = _.mapValues(groupByData, function(dataHorasObjArray) {
        return _.flatten(dataHorasObjArray.spread('horas'));
    });
    return horasPorDataObj;
};

RegrasStiff.prototype.calculaHorasTrabalhadas = function(horasPorDataObj, considerarDataCorrente) {
    return _.reduce(horasPorDataObj , function(acumulador, horas, data) {
        var horasDia = this.calculaHorasTrabalhadasDia(data, horas, considerarDataCorrente);
        return acumulador.plus(horasDia);
    }, new Time(), this);
};

RegrasStiff.prototype.calculaHorasTrabalhadasDia = function(data, horas, considerarDataCorrente) {
    var day = parseInt(data.substr(0, 2));
    var month = parseInt(data.substr(3, 2)) - 1;
    var year = parseInt(data.substr(6, 4));
    new Date(year, month, day, 0, 0, 0, 0);

    //var ehHoje = Date.fromStringDMY(data).isToday();
    var ehHoje = new Date(year, month, day, 0, 0, 0, 0).isToday();

    if (!ehHoje || (ehHoje && considerarDataCorrente)) {
        var horasTrabalhadas = this.somaIntervalos(horas);
        var horasAjuste = RegrasAjuste.calculaHorasAjusteTotal(data, horas);
        return horasTrabalhadas.plus(horasAjuste);
    }

    return new Time(0, 0);
};

RegrasStiff.prototype.calculaHorasTeoricas = function(horasPorDataObj, considerarDataCorrente) {
    var horasTeoricasDia = new Time(8);
    return _.reduce(horasPorDataObj , function(acumulador, horas, data) {
        var horasDia = this.calculaHorasTrabalhadasDia(data, horas, considerarDataCorrente);
        return horasDia.inMinutes() > 0 ?
            acumulador.plus(horasTeoricasDia) :
            acumulador;
    }, new Time(), this);
};

RegrasStiff.prototype.somaIntervalos = function(arrayEntradasESaidasString) {
    var arrayClone = arrayEntradasESaidasString.clone();
    var duracaoIntervalos = new Time();
    while(arrayClone.length >= 1) {
        try {
            var entradaString = arrayClone.shift();
            var saidaString   = arrayClone.shift();
            var entrada = Time.fromString(entradaString);
            var saida;
            try {
                saida = Time.fromString(saidaString);
            } catch(any) {
                saida = Time.now();
            }
            if(saida.inMinutes() > entrada.inMinutes()) {
                duracaoIntervalos = duracaoIntervalos
                    .plus(saida)
                    .minus(entrada);
            }
        } catch(any) {}
    }
    return duracaoIntervalos;
};


function RegrasAjuste() {}

RegrasAjuste.prototype.calculaHorasDescontoAlmoco = function(data, horas) {
    var inicio = new Time(11, 45);
    var fim = new Time(13, 15);

    var horasFiltradas = _.map(horas, function(hora) {
        if (hora === "-") {
            return null;
        }

        hora = Time.fromString(hora);
        if (hora.inMinutes() >= inicio.inMinutes() && hora.inMinutes() <= fim.inMinutes()) {
            return hora;
        } else {
            return null;
        }
    });

    var mapaPorTipo = _.groupBy(horasFiltradas, function(hora) {
        if (hora === null) {
            return 'nulo';
        } else if ((horasFiltradas.indexOf(hora) % 2) == 0) {
            return 'entrada';
        } else {
            return 'saida';
        }
    });

    if (mapaPorTipo.entrada === undefined) {
        mapaPorTipo.entrada = [fim];
    }
    if (mapaPorTipo.saida === undefined) {
        mapaPorTipo.saida = [inicio];
    }

    if (mapaPorTipo.entrada.length > mapaPorTipo.saida.length) {
        mapaPorTipo.saida.push(inicio);
    } else if (mapaPorTipo.entrada.length < mapaPorTipo.saida.length) {
        mapaPorTipo.entrada.push(fim);
    }

    var ordenado = _.sortBy(_.flatten([mapaPorTipo.entrada, mapaPorTipo.saida]), function(hora) {
        return hora.inMinutes();
    });

    var emString = _.map(ordenado, function(hora) {
        return hora.toString();
    });

    var tempoDeAlmoco = new RegrasStiff().somaIntervalos(emString);

    var ajusteEmMinutos = 0;
    if (tempoDeAlmoco.inMinutes() < 60) {
        ajusteEmMinutos = tempoDeAlmoco.inMinutes() - 60;
    }

    return Time.fromMinutes(ajusteEmMinutos);
};

RegrasAjuste.prototype.calculaHorasAjusteHorarioNucleo = function(data, horas) {
    return new Time(0,0);
};

RegrasAjuste.calculaHorasAjusteTotal = function(data, horas) {
    var r = new RegrasAjuste();
    return _.reduce(_.functions(r) , function(acumulador, func) {
        var desconto = this[func](data, horas);
        return acumulador.plus(desconto);
    }, new Time(), r);
};

function PTEDecorator() {}

PTEDecorator.prototype.calculaHorasTrabalhadas = function () {
    var regrasStiff = new RegrasStiff();
    this.adicionaColunaHorasTrabalhadas(regrasStiff);
    this.adicionaOpcaoMostrarCatraca();
    this.calculaTotalHorasTrabalhadas(regrasStiff);
    this.iniciaKeepAlive();
};

PTEDecorator.prototype.adicionaColunaHorasTrabalhadas = function (regrasStiff) {
    $('#ContentPlaceHolder1_gvMarcacao tr:first-child').append('<th scope="col">Horas</th>');
    $('#ContentPlaceHolder1_gvMarcacao tr.BgTrPair, #ContentPlaceHolder1_gvMarcacao tr.BgTrOdd').each(function () {
        var horas = [
            $(this).find('td:nth-child(2) .hora').text().trim(),
            $(this).find('td:nth-child(3) .hora').text().trim(),
            $(this).find('td:nth-child(4) .hora').text().trim(),
            $(this).find('td:nth-child(5) .hora').text().trim()
        ];

        var data = $(this).find('.data').text();
        var horasTrabalhadas = regrasStiff.calculaHorasTrabalhadasDia(data, horas, true);
        $(this).closest('tr').append('<td>' + horasTrabalhadas.toString() + '</td>');
    });
};

PTEDecorator.prototype.calculaTotalHorasTrabalhadas = function (regrasStiff) {
    var entradas = $('#ContentPlaceHolder1_gvMarcacao tr.BgTrPair, #ContentPlaceHolder1_gvMarcacao tr.BgTrOdd').map(function () {
        var data = $(this).find('.data').text();
        var horas = [
            $(this).find('td:nth-child(2) .hora').text().trim(),
            $(this).find('td:nth-child(3) .hora').text().trim(),
            $(this).find('td:nth-child(4) .hora').text().trim(),
            $(this).find('td:nth-child(5) .hora').text().trim()
        ];
        return {
            data: data,
            horas: horas
        };
    }).get();

    var horasPorDataObj = regrasStiff.agrupaHorasPorData(entradas);
    
    var horasTrabalhadasComDataCorrente = regrasStiff.calculaHorasTrabalhadas(horasPorDataObj, true);
    var horasTeoricasComDataCorrente = regrasStiff.calculaHorasTeoricas(horasPorDataObj, true);
    var balancoComDataCorrente = horasTrabalhadasComDataCorrente.minus(horasTeoricasComDataCorrente);
    
    var horasTrabalhadasSemDataCorrente = regrasStiff.calculaHorasTrabalhadas(horasPorDataObj, false);
    var horasTeoricasSemDataCorrente = regrasStiff.calculaHorasTeoricas(horasPorDataObj, false);
    var balancoSemDataCorrente = horasTrabalhadasSemDataCorrente.minus(horasTeoricasSemDataCorrente);
    
    var balancoDoDia = balancoComDataCorrente.minus(balancoSemDataCorrente);
    var saidaEsperada = new Date();
    saidaEsperada = new Time(saidaEsperada.getHours(), saidaEsperada.getMinutes()).minus(balancoDoDia);
    
    if (balancoDoDia.negative === true) {
        saidaEsperada =  ' (saída as ' + saidaEsperada.toString()  + ')';
    } else {
        saidaEsperada = '';
    }
    
    $('.ObservacaoConteudo > .linha:last-child').after(
        '<div class="linha">' +
        '<div class="coluna Dupla"><label for="nomeCurso">Balanço no mês:</label><span id="ContentPlaceHolder1_lblPeriodo">&nbsp;' +
        balancoComDataCorrente.toString() + ' (' + balancoSemDataCorrente.toString() + ')' +
        '</span></div>' + 
        '<div class="coluna Dupla"><label for="nomeCurso">Balanço no dia:</label><span id="ContentPlaceHolder1_lblPeriodo">&nbsp;' +
        balancoDoDia.toString() + saidaEsperada +
        '</span></div>' +
        '</div>');
        
    var negative = false;    
    var balancoTotal = localStorage.getItem('PTEDecorator.balancoTotal');
    if (balancoTotal[0] === '-') {
        balancoTotal = balancoTotal.replace('-', '');
        negative = true;
    }
    balancoTotal = balancoTotal ? balancoTotal : '00:00';
    balancoTotal = Time.fromString(balancoTotal);
    balancoTotal.negative = negative;    
        
    $('.ObservacaoConteudo > .linha:last-child').after(
        '<div class="linha">' +
        '<div class="coluna Dupla"><label for="nomeCurso">Balanço total:</label><span id="ContentPlaceHolder1_lblPeriodo">&nbsp;' +
        balancoTotal.plus(balancoComDataCorrente).toString() + ' (' + balancoTotal.plus(balancoSemDataCorrente).toString() + ')' +
        '</span></div>' +
        '</div>');
};

PTEDecorator.prototype.adicionaOpcaoMostrarCatraca = function () {
    $('.ObservacaoConteudo > .linha:last-child').append('<div class="coluna Dupla"><label for="mostrarCatraca">Mostrar catraca?</label><input type="checkbox" id="mostrarCatraca"></input></div>');
    var checkbox = $("#mostrarCatraca");
    var pteDecorator = this;
    checkbox.change(function () {
        var selecionouMostrar = $(this).prop("checked");
        $(".catraca").toggle(selecionouMostrar);
        pteDecorator.setMostrarCatraca(selecionouMostrar);
    });
    var mostrar = pteDecorator.getMostrarCatraca();
    checkbox.prop("checked", mostrar);
    $(".catraca").toggle(mostrar);
};

PTEDecorator.prototype.getMostrarCatraca = function () {
    return localStorage.getItem("PTEDecorator.mostrarCatraca") === "true";
};

PTEDecorator.prototype.setMostrarCatraca = function (valor) {
    return localStorage.setItem("PTEDecorator.mostrarCatraca", valor);
};

PTEDecorator.prototype.iniciaKeepAlive = function () {
    setInterval(function() {
        $("#btnConsultar").click();
    }, 5 * 60 * 1000);
};

new PTEDecorator().calculaHorasTrabalhadas();
